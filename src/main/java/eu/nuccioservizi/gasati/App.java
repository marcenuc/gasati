package eu.nuccioservizi.gasati;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;
import com.google.api.services.sheets.v4.model.GridRange;
import com.google.api.services.sheets.v4.model.ValueRange;

/*
 * Documentazione per le API di Google utilizzate:
 * 
 * - Drive: https://developers.google.com/drive/v3/web/about-sdk
 * - Sheets: https://developers.google.com/sheets/api/
 */
public class App {
	private static final String APPLICATION_NAME = "Gasati";
	private static final String NOME_ORDINE = "Santa Barbara";

	/**
	 * Global instance of the scopes required by this App.
	 *
	 * If modifying these scopes, delete your previously saved credentials at
	 * ~/.credentials/eu.nuccioservizi.gasati
	 */
	private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE, SheetsScopes.SPREADSHEETS);

	private final JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
	private final HttpTransport httpTransport;
	private final FileDataStoreFactory dataStoreFactory;
	private Sheets sheetsService = null;
	private Drive driveService = null;

	private App(HttpTransport httpTransport, FileDataStoreFactory dataStoreFactory) {
		this.httpTransport = httpTransport;
		this.dataStoreFactory = dataStoreFactory;
	}

	public static App build() throws GeneralSecurityException, IOException {
		final java.io.File dataStoreDir = new java.io.File(System.getProperty("user.home"),
				".credentials/eu.nuccioservizi.gasati");

		return new App(GoogleNetHttpTransport.newTrustedTransport(), new FileDataStoreFactory(dataStoreDir));
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @return an authorized Credential object.
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public Credential authorize() throws IOException {
		// Load client secrets.
		final InputStream in = App.class.getResourceAsStream("/client_secret.json");
		final GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(getJsonFactory(), new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(getHttpTransport(),
				getJsonFactory(), clientSecrets, SCOPES).setDataStoreFactory(getDataStoreFactory())
						.setAccessType("offline").build();

		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	private FileDataStoreFactory getDataStoreFactory() {
		return this.dataStoreFactory;
	}

	private HttpTransport getHttpTransport() {
		return this.httpTransport;
	}

	private JsonFactory getJsonFactory() {
		return this.jsonFactory;
	}

	/**
	 * Build and return an authorized Drive client service.
	 * 
	 * @return an authorized Drive client service
	 * @throws IOException
	 */
	public synchronized Drive getDriveService() throws IOException {
		if (this.driveService == null)
			this.driveService = new Drive.Builder(getHttpTransport(), getJsonFactory(), authorize())
					.setApplicationName(APPLICATION_NAME).build();

		return this.driveService;
	}

	public synchronized Sheets getSheetsService() throws IOException {
		if (this.sheetsService == null)
			this.sheetsService = new Sheets.Builder(getHttpTransport(), getJsonFactory(), authorize())
					.setApplicationName(APPLICATION_NAME).build();

		return this.sheetsService;
	}

	private static final String ID_AND_PERMISSIONS_FIELD = "id,permissions";
	/** L'ID del file da cui creare le copie per i singoli gasisti. */
	private static final String ID_LISTINO = "1Toqw5MvV0cpi655qnWuWX44bi72DcsQ1pkY_zwcrUsE";
	/** L'ID della cartella che contiene il listino e le copie per gli ordini. */
	private static final String ID_CARTELLA_ORDINE = "15ODosqxAU6f0s_rdJCbgqcuu67VElN8d";

	/** Nome del campo con i permessi di un file. */
	private static final String PERMISSIONS = "permissions";

	/** Prefisso usato nel nome delle copie del listino. */
	private static final String PREFISSO_COPIA_LISTINO = "Ordine di ";

	/** Usato solo per fare delle prove. */
	private static final String ID_FILE_PROVA = "1l3jJE55oytZaIArLyPPnl-muMny3mTRtJiZZwL16taY";

	/** Cerca tutti i file con gli ordini dei gasisti. */
	private static final String QUERY_COPIA_LISTINO = "'" + ID_CARTELLA_ORDINE + "' in parents and name contains '"
			+ PREFISSO_COPIA_LISTINO + "' and mimeType = 'application/vnd.google-apps.spreadsheet' and trashed = false";

	/**
	 * Verifica se permission rappresenta il proprietario.
	 * 
	 * @param permission
	 *            I permessi da verificare.
	 * @return TRUE se e solo se sono i permessi del proprietario.
	 */
	private static boolean isOwner(final Permission permission) {
		return "owner".equals(permission.getRole());
	}

	/**
	 * Crea una copia del listino con un nome univoco e i permessi di scrittura solo
	 * per il gasista identificato da permission.
	 * 
	 * @param permission
	 *            Il gasista che potrà usare la copia creata per fare il suo ordine.
	 * @throws IOException
	 */
	private void copiaListino(final Permission permission, final Set<String> copieEsistenti) throws IOException {
		if (isOwner(permission))
			return; // il proprietario non fa l'ordine, è solo un referente.

		final Drive service = getDriveService();

		final String nomeCopia = nomeCopiaListino(permission);

		if (copieEsistenti.contains(nomeCopia)) {
			System.out.println(nomeCopia + " esiste già");
			return; // Il file esiste già (magari perché la procedura è stata interrotta)
		}

		// Copia il listino e torna ID e permessi
		final File copia = service.files().copy(ID_LISTINO, new File().setName(nomeCopia))
				.setFields(ID_AND_PERMISSIONS_FIELD).execute();

		System.out.println(copia.toPrettyString());

		/*
		 * TODO: è IMPORTANTE migliorare questa parte perché fa molte richieste alle API
		 * di Google, quindi è lenta, e consuma molte risorse (le API hanno limiti sul
		 * numero di chiamate che si possono effettuare).
		 * 
		 * Quello che fa è rimuovere i permessi sulla copia del listino per tutti tranne
		 * il proprietario (cioè il referente) e il gasista. Non ho trovato il modo di
		 * fare questo con una sola chiamata perché sono riuscito a modificare solo un
		 * permesso alla volta.
		 * 
		 * N.B.: i permessi della copia sono gli stessi del file originale.
		 */
		for (final Permission p : copia.getPermissions())
			if (!isOwner(p) && !p.getEmailAddress().equals(permission.getEmailAddress()))
				service.permissions().delete(copia.getId(), p.getId()).execute();
	}

	/**
	 * Crea un file per ogni gasista in cui può caricare il suo ordine.
	 * 
	 * @throws IOException
	 *             Per problemi di connessione
	 */
	private void creaCopieListino() throws IOException {
		// Possono fare l'ordine gli utenti che hanno accesso al listino,
		// quindi usiamo questa lista per creare i file per i singoli gasisti.
		final List<Permission> permessi = getDriveService().files().get(ID_LISTINO).setFields(PERMISSIONS).execute()
				.getPermissions();

		final Set<String> copieEsistenti = nomiFileOrdiniEsistenti();

		for (final Permission permission : permessi)
			copiaListino(permission, copieEsistenti);
	}

	/**
	 * Crea un nome univoco per l'ordine del gasista identificato da permission.
	 * 
	 * @param permission
	 *            Il gasista che userà questo file per fare l'ordine.
	 * @return Il nome da dare al file.
	 */
	private static String nomeCopiaListino(final Permission permission) {
		return PREFISSO_COPIA_LISTINO + permission.getEmailAddress();
	}

	/**
	 * Estrae l'email del gasista dal nome del file con l'ordine.
	 * 
	 * @param nomeFile
	 *            Il nome del file con l'ordine
	 * @return La mail del gasista
	 */
	private static String emailCopiaListino(final String nomeFile) {
		return nomeFile.substring(PREFISSO_COPIA_LISTINO.length());
	}

	/**
	 * Trova gli intervalli in cui il gasista ha inserito il suo ordine.
	 * 
	 * @param fileId
	 *            ID del file con l'ordine.
	 * @return Gli intervalli contenenti i valori inseriti nell'ordine.
	 * @throws IOException
	 */
	private List<GridRange> getInputRanges(final String fileId) throws IOException {
		// Le celle di input sono quelle in intervalli non protetti da scrittura
		final List<GridRange> inputRanges = getSheetsService().spreadsheets().get(fileId)
				.setFields("sheets/protectedRanges/unprotectedRanges").execute().getSheets().get(0).getProtectedRanges()
				.get(0).getUnprotectedRanges();

		// Ordiniamole in base alla prima riga, così manteniamo l'ordine del file
		inputRanges.sort((GridRange o1, GridRange o2) -> o1.getStartRowIndex() < o2.getStartRowIndex() ? -1
				: o1.getStartRowIndex() > o2.getStartRowIndex() ? 1 : 0);

		return inputRanges;
	}

	/**
	 * Accrocchio per semplificare la conversione in testo degli indici di colonna
	 * in un intervallo.
	 * 
	 * N.B.: si può trovare una soluzione, ma è poco importante perché in un ordine
	 * il numero di colonne usate è più o meno costante.
	 */
	private static final String[] A1_COLUMNS = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M" };

	/**
	 * Converte un intervallo di celle dal formato "binario" (indici di inizio e
	 * fine per righe e colonne) al formato "A1" (quello usato nelle formule del
	 * foglio di calcolo).
	 * 
	 * Esempio: l'intervallo (righe: (da: 0, a: 2), colonne: (da: 1, a: 1)) diventa
	 * "B1:B2".
	 * 
	 * N.B.: questa funzione NON funziona in tutti i casi d'uso, è utile solo per
	 * questa specifica applicazione. Non perdo tempo a farla meglio perché spero di
	 * trovare il modo di farne a meno (vedi TODO qui sotto).
	 * 
	 * TODO: questa funzione esiste solo perché c'è un'inconsistenza nelle API di
	 * Google. Nelle risposte le API ritornano intervalli "binari", ma nelle query
	 * richiedono intervalli "A1". Se si trova il modo di poter usare sempre lo
	 * stesso formato, si elimina la necessita di fare conversioni e anche la
	 * possibilità di sbagliare.
	 * 
	 * @param range
	 *            L'intervallo da convertire
	 * @return La rappresentazione in formato "A1" dell'intervallo, con anche il
	 *         nome del foglio.
	 */
	private static String gridRangeToA1Range(final GridRange range) {
		// TODO: prendere il nome del foglio dal file
		final StringBuilder a1Range = new StringBuilder("Ordine!") //
				.append(A1_COLUMNS[range.getStartColumnIndex()]) //
				.append(range.getStartRowIndex() + 1);

		/*
		 * Verifica se l'intervallo ha più di una cella per non scrivere, per esempio,
		 * "A1:A1". Questo è importante perché, anche se "A1:A1" è perfettamente valido
		 * e funziona, non viene riconosciuto da alcune API come equivalente ad "A1",
		 * producendo strani malfunzionamenti.
		 */
		if (range.getEndColumnIndex() - range.getStartColumnIndex() > 1
				|| range.getEndRowIndex() - range.getStartRowIndex() > 1)
			a1Range.append(":") // aggiungiamo l'altro angolo dell'intervallo
					.append(A1_COLUMNS[range.getEndColumnIndex() - 1]) //
					.append(range.getEndRowIndex());

		return a1Range.toString();
	}

	/**
	 * Estrae i valori contenuti negli intervalli indicati.
	 * 
	 * @param fileId
	 *            ID del file da cui estrarre i valori.
	 * @param ranges
	 *            Gli intervalli di interesse.
	 * @return I valori NON FORMATTATI contenuti negli intervalli.
	 * @throws IOException
	 */
	private BatchGetValuesResponse getRanges(final String fileId, final List<String> ranges) throws IOException {
		return getSheetsService().spreadsheets().values().batchGet(fileId).setRanges(ranges)
				.setValueRenderOption(/* ValueRenderOption. */"UNFORMATTED_VALUE")
				.setFields("valueRanges(range,values)").execute();
	}

	/**
	 * Dato un intervallo di celle con le quantità ordinate, calcola l'intervallo
	 * corrispondente, in formato "A1", dei relativi prodotti con codici e
	 * descrizioni.
	 * 
	 * @param qtaRange
	 *            L'intervallo di celle in formato "binario" con le quantità.
	 * @return L'intervallo di celle in formato "A1" con i prodotti.
	 */
	private static String qtaGridRangeToA1ProdottoRange(final GridRange qtaRange) {
		return gridRangeToA1Range(qtaRange.clone().setStartColumnIndex(1).setEndColumnIndex(6));
	}

	private void creaOrdineFinale(final String idOrdine) throws IOException {
		final List<GridRange> inputRanges = getInputRanges(ID_LISTINO);

		final List<String> qtaRanges = new ArrayList<>(inputRanges.size());
		final Map<String, List<List<Object>>> prodotti = new HashMap<>(inputRanges.size());
		{
			final List<String> prodottiRanges = new ArrayList<>(inputRanges.size());
			final Map<String, String> prodottiRangesQtaRanges = new HashMap<>(inputRanges.size());
			for (final GridRange r : inputRanges) {
				final String prodottoRange = qtaGridRangeToA1ProdottoRange(r);
				final String qtaRange = gridRangeToA1Range(r);
				qtaRanges.add(qtaRange);
				prodottiRanges.add(prodottoRange);
				prodottiRangesQtaRanges.put(prodottoRange, qtaRange);
			}

			for (final ValueRange r : getRanges(ID_LISTINO, prodottiRanges).getValueRanges()) {
				prodotti.put(prodottiRangesQtaRanges.get(r.getRange()), r.getValues());
			}
		}

		final Map<String, Map<String, int[]>> ordini;
		if (idOrdine == null)
			ordini = estraiOrdini(qtaRanges);
		else {
			ordini = new HashMap<>(1);
			ordini.put(idOrdine, estraiQuantità(qtaRanges, idOrdine));
		}

		final String[] gasisti = ordini.keySet().toArray(new String[0]);
		Arrays.sort(gasisti);

		try (final CSVPrinter csv = CSVFormat.DEFAULT
				.print(Files.newBufferedWriter(Paths.get("ORDINE " + NOME_ORDINE + ".csv"), StandardCharsets.UTF_8))) {

			csv.printRecord((Object[]) gasisti);

			int rangeNum = 0;

			for (final GridRange range : inputRanges) {
				final String qtaRange = qtaRanges.get(rangeNum++);

				for (int rowNum = 0, i = range.getStartRowIndex(); i < range.getEndRowIndex(); ++i, ++rowNum) {
					for (final Object gasista : gasisti) {
						final int[] qtas = ordini.get(gasista).get(qtaRange);
						csv.print((qtas == null || qtas.length <= rowNum || qtas[rowNum] == 0) ? ""
								: Integer.toString(qtas[rowNum]));
					}

					for (final Object v : prodotti.get(qtaRange).get(rowNum))
						csv.print(v.toString().trim());

					csv.println();
				}
			}
		}

		try (final CSVPrinter csv = CSVFormat.DEFAULT
				.print(Files.newBufferedWriter(Paths.get("ORDINI " + NOME_ORDINE + ".csv"), StandardCharsets.UTF_8))) {
			for (final String gasista : gasisti) {
				csv.print(gasista);
				csv.println();

				int rangeNum = 0;

				for (final GridRange range : inputRanges) {
					final String qtaRange = qtaRanges.get(rangeNum++);

					for (int rowNum = 0, i = range.getStartRowIndex(); i < range.getEndRowIndex(); ++i, ++rowNum) {
						final int[] qtas = ordini.get(gasista).get(qtaRange);
						if (qtas != null && qtas.length > rowNum && qtas[rowNum] != 0) {
							csv.print(Integer.toString(qtas[rowNum]));
							for (final Object v : prodotti.get(qtaRange).get(rowNum))
								csv.print(v.toString().trim());
							csv.println();
						}
					}
				}

				csv.println();
			}
		}
	}

	private FileList listaFileOrdini(final String pageToken) throws IOException {
		return getDriveService().files().list() //
				.setCorpora("user") //
				.setQ(QUERY_COPIA_LISTINO) //
				.setSpaces("drive") //
				.setFields("nextPageToken, files(id, name)") //
				.setPageToken(pageToken) //
				.execute();
	}

	private Set<String> nomiFileOrdiniEsistenti() throws IOException {
		final Set<String> nomi = new HashSet<>();

		String pageToken = null;
		do {
			final FileList list = listaFileOrdini(pageToken);

			for (final File file : list.getFiles()) {
				nomi.add(file.getName());
			}

			pageToken = list.getNextPageToken();
		} while (pageToken != null);

		return nomi;
	}

	private Map<String, Map<String, int[]>> estraiOrdini(final List<String> qtaRanges) throws IOException {
		final Map<String, Map<String, int[]>> ordini = new HashMap<>();

		String pageToken = null;
		do {
			final FileList list = listaFileOrdini(pageToken);

			for (final File file : list.getFiles()) {
				final Map<String, int[]> qtas = estraiQuantità(qtaRanges, file.getId());

				if (!qtas.isEmpty())
					ordini.put(emailCopiaListino(file.getName()), qtas);
			}

			pageToken = list.getNextPageToken();
		} while (pageToken != null);

		return ordini;
	}

	private Map<String, int[]> estraiQuantità(final List<String> qtaRanges, final String fileId) throws IOException {
		final Map<String, int[]> qtas = new HashMap<>(qtaRanges.size());

		for (final ValueRange r : getRanges(fileId, qtaRanges).getValueRanges()) {
			final List<List<Object>> values = r.getValues();
			if (values != null) {
				// N.B.: values contiene solo i valori inseriti, quindi può essere più corto
				// dell'intervallo di valori richiesto
				final int[] valori = new int[values.size()];
				int i = 0;
				for (final List<Object> o : values)
					valori[i++] = o.isEmpty() ? 0 : parseInt(o.get(0));

				qtas.put(r.getRange(), valori);
			}
		}

		return qtas;
	}

	/**
	 * Converte l'oggetto in un intero partendo dalla sua rappresentazione come
	 * stringa.
	 * 
	 * @param o
	 *            L'oggetto da convertire
	 * @return 0 se o.toString() contiene caratteri che non sono cifre, altrimenti
	 *         il valore convertito in int letto in base 10.
	 */
	public static int parseInt(final Object o) {
		final String s = o.toString().trim();
		return s.matches("^[0-9]+$") ? Integer.parseInt(s) : 0;
	}

	public static void main(String[] args) throws IOException, GeneralSecurityException {
		final App app = App.build();

		// ATTENZIONE: usare solo uno dei comandi seguenti
		// TODO: creare un'interfaccia per chiedere cosa fare senza ricompilare

		// Usare questo per "aprire" l'ordine
		app.creaCopieListino();
		// Oppure questo per fare una prova con un solo file
		// app.creaOrdineFinale(ID_FILE_PROVA);
		// Oppure questo per creare l'ordine finale
		// app.creaOrdineFinale(null);
	}
}